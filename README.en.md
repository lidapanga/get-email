# 获取邮箱邮件

#### Description
该项目是以QQ邮箱为例
pop3:pop3允许电子邮件客户端请求下载指定邮件服务器上指定用户的邮件信息，
      但是在用户的客户端所做的任何操作都是不会反馈到服务器上的，也就是说，
      你已读了邮件在邮件服务器上的状态还是未读取的，
      这在很多情况下对用户来说是不方便的。这是因为pop3协议是单向协议
imap:IAMP协议，双向协议，用户在客户端的操作可以实时的反馈到服务器上，用户对邮件的任何操作，服务器也会做出相应的操作
     同时：IMAP还可以指定下载邮件的某些内容。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
