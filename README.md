# 获取邮箱邮件

#### 介绍
该项目是以QQ邮箱为例
pop3:pop3允许电子邮件客户端请求下载指定邮件服务器上指定用户的邮件信息，
      但是在用户的客户端所做的任何操作都是不会反馈到服务器上的，也就是说，
      你已读了邮件在邮件服务器上的状态还是未读取的，
      这在很多情况下对用户来说是不方便的。这是因为pop3协议是单向协议
imap:IAMP协议，双向协议，用户在客户端的操作可以实时的反馈到服务器上，用户对邮件的任何操作，服务器也会做出相应的操作
     同时：IMAP还可以指定下载邮件的某些内容。

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
